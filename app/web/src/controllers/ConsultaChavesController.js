salt.controller('ConsultaChavesController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {
    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

    $scope.consultaChave = function () {


        StateService.getMappingMappingState("banks", 
        $scope.bancoOrigemSelecionado, 
        "encryptedSharedSymKeys", 
        $scope.bancoDestinoSelecionado).then(function (ret){

            chaveSimetricaCripto = ret.encryptedCentralBankTransactionSharedSymKey.replace(/\u0000/g,'');
            StateService.getMappingState("banks",enderecoBCB).then(function(cert){
                certificadoBCB = cert.certificate.replace(/\u0000/g,'');
                $q.when(enveloped_DECRYPT(certificadoBCB, $scope.chavePrivada, chaveSimetricaCripto)).then(function (senha){
                    console.log(senha);
                    $scope.chaveEmClaro = senha;
                });
            });

        });
    }

});