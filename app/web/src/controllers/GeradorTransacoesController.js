salt.controller('GeradorTransacoesController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {
    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

    $scope.geraTransacoes = function () {

        var nomeBanco = _.find($scope.bancos, function (item) {
            return item.addrBank == $scope.bancoOrigemSelecionado;
        }).name;

        var bancos = _.clone($scope.bancos);
        _.remove(bancos, function (item) {
            return item.addrBank == $scope.bancoOrigemSelecionado || (item.addrBank == nomeUsuarioBCB);
        });

        var transacoes = []

        _.times($scope.qtde, function (i) {
            transacoes.push({
                destino: _.sample(bancos).addrBank,
                valor: _.random(1, 1000000)
            })
        });

        var geraTransacoes = _.map(transacoes, function (trans) {
            return StateService.getMappingMappingState("banks",
                $scope.bancoOrigemSelecionado,
                "encryptedSharedSymKeys",
                trans.destino).then(function (ret) {

                    chaveSimetricaCripto = ret.encryptedTransactionSharedSymKey.replace(/\u0000/g, '');
                    StateService.getMappingState("banks", $scope.bancoOrigemSelecionado).then(function (cert) {
                        certificadoOrigem = cert.certificate.replace(/\u0000/g, '');
                        $q.when(enveloped_DECRYPT(certificadoOrigem, $scope.chavePrivadaOrigem, chaveSimetricaCripto)).then(function (senha) {
                            console.log(senha);
                            console.log(trans.valor);
                            //$scope.valor
                            valorCripto = CryptoJS.AES.encrypt(trans.valor.toString(), senha).toString();
                            console.log(trans.destino + ":" + trans.valor + ":" + valorCripto);
                            TransactionService.executaTransacao(nomeBanco, $scope.bancoOrigemSelecionado, $scope.senhaCarteira, "createTransaction",
                                JSON.stringify({
                                    "_rcptBank": trans.destino,
                                    "_value": trans.valor,
                                    "_transactionEncryptedData": valorCripto
                                })
                            ).then(function (ret) {
                                console.log(ret);
                            }, function (err) {
                                console.log(err);
                            });

                        });
                    });

                });

        });


    }
});

