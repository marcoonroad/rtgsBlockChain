salt.controller('CadastraChavesController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {
    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

    $scope.cadastraChave = function () {
        //Buscar o certificado de cada um
        var chaveCompartilhadaCripto = {};
        var certificados = {};

        var getCertificates = _.map(
            [
                $scope.bancoOrigemSelecionado,
                $scope.bancoDestinoSelecionado,
                enderecoBCB
            ], function (item) {

                return StateService.getMappingState("banks", item).then(function (data) {
                    certificados[item] = data.certificate.replace(/\u0000/g, '');

                });

            });

        $q.all(getCertificates).then(function () {

            var getState = _.map(
                [
                    $scope.bancoOrigemSelecionado,
                    $scope.bancoDestinoSelecionado,
                    enderecoBCB
                ], function (item) {
                    return $q.when(enveloped_ENCRYPT(certificados[item], "alg_CBC", "len_128",
                        $scope.chaveCompartilhada)).then(function (cript) {
                            chaveCompartilhadaCripto[item] = cript;
                        });
                });

            $q.all(getState).then(function () {

                console.log(chaveCompartilhadaCripto[$scope.bancoOrigemSelecionado]);

                TransactionService.executaTransacao(nomeUsuarioBCB, enderecoBCB, $scope.senhaCarteiraBCB, "addSharedSymmetricKeys",
                    JSON.stringify({
                        "_senderBank": $scope.bancoOrigemSelecionado,
                        "_rcptBank": $scope.bancoDestinoSelecionado,
                        "_encryptedSenderTrSymKey": chaveCompartilhadaCripto[$scope.bancoOrigemSelecionado],
                        "_encryptedRcptTrSymKey": chaveCompartilhadaCripto[$scope.bancoDestinoSelecionado],
                        "_encryptedCentralBankTrSymKey": chaveCompartilhadaCripto[enderecoBCB]
                    })
                ).then(function (ret) {
                    console.log("Cadastrado");
                }, function (err) {
                    console.log(err);
                });
            });
        });





        //StateService.getMappingState("banks",$scope.bancoOrigemSelecionado)


    }
});