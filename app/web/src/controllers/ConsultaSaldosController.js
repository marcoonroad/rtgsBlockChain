salt.controller('ConsultaSaldosController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {

    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {
            $scope.bancos = data.banksWeb;
        });
    }
    buscaIFs();

    var buscaTodosIDs = function () {
        var deferred = $q.defer();
        StateService.getFullState().then(function (state) {
            var ultimoID = parseInt(state.totalTransactions);
            deferred.resolve(_.remove(_.range(1, ultimoID + 1), function (x) { return x > 0 }));

        });
        return deferred.promise;
    }

    $scope.calculaSaldos = function () {

        //$scope.bancos = [];
        //var saldos = {};
        $scope.ultimasTransacoes = [];
        var saldoPrevisto = {};

        var ehBancoCentral = true;// ($scope.bancoOrigemSelecionado == enderecoBCB);

        //var funcaoBuscaIDs = ehBancoCentral ? buscaUltimosIDs() : StateService.getMappingState("bankTransactions", $scope.bancoOrigemSelecionado);
        //buscaIFs();



        buscaTodosIDs().then(function (ids) {
            StateService.getMappingState("banks", enderecoBCB).then(function (cert) {
                certificadoOrigem = cert.certificate.replace(/\u0000/g, '');
                var buscaSaldosIniciais = _.map($scope.bancos, function (banco) {

                    if (banco.addrBank != enderecoBCB) {
                        return StateService.getMappingState("banks", banco.addrBank).then(function (item) {

                            var saldoInicialCripto = item.encryptedBalanceForCentralBank;
                            return $q.when(enveloped_DECRYPT(certificadoOrigem, $scope.chavePrivadaOrigem, saldoInicialCripto)).then(function (saldo) {
                                banco.saldoInicial = parseInt(saldo);
                                banco.saldo = 0;
                                banco.debitoPrevisto = 0;

                            });

                        });
                    }

                });
                var buscaUltimas = _.map(ids, function (id) {
                    return StateService.getMappingState("transactionLog", id).then(function (trans) {
                        //obj.id = id;
                        //ultimasTransacoes.push(obj);
                        enderecoBancoOrigem = trans.senderBank;
                        enderecoBancoDestino = trans.rcptBank;
                        //if (trans.confirmed) {
                        return StateService.getMappingMappingState("banks",
                            enderecoBancoOrigem,
                            "encryptedSharedSymKeys",
                            enderecoBancoDestino).then(function (ret) {

                                if (ehBancoCentral) {
                                    chaveSimetricaCripto = ret.encryptedCentralBankTransactionSharedSymKey.replace(/\u0000/g, '');
                                } else {
                                    chaveSimetricaCripto = ret.encryptedTransactionSharedSymKey.replace(/\u0000/g, '');
                                }

                                return $q.when(enveloped_DECRYPT(certificadoOrigem, $scope.chavePrivadaOrigem, chaveSimetricaCripto)).then(function (senha) {

                                    bancoOrigem = _.find($scope.bancos, function (b) { return b.addrBank == trans.senderBank });
                                    bancoDestino = _.find($scope.bancos, function (b) { return b.addrBank == trans.rcptBank });

                                    //if (!bancoOrigem.saldo) bancoOrigem.saldo = 0;
                                    //if (!bancoDestino.saldo) bancoDestino.saldo = 0;

                                    var valor = parseInt(CryptoJS.AES.decrypt(trans.transactionEncryptedData.replace(/\u0000/g, ''), senha).toString(CryptoJS.enc.Utf8));

                                    if (!isNaN(valor)) {
                                        if (trans.confirmed) {
                                            bancoOrigem.saldo -= valor;
                                            bancoDestino.saldo += valor;
                                        }
                                    }
                                    if (!trans.confirmed) {

                                        $scope.ultimasTransacoes.push({
                                            id: parseInt(id),
                                            enderecoBanco: trans.rcptBank,
                                            nomeBancoOrigem: _.find($scope.bancos, function (b) { return b.addrBank == trans.senderBank }).name,
                                            nomeBancoDestino: _.find($scope.bancos, function (b) { return b.addrBank == trans.rcptBank }).name,
                                            confirmada: trans.confirmed,
                                            valorDescriptografado: valor,
                                            permitida: trans.allowed,
                                            icone: trans.allowed ? "fa-lock" : "fa-unlock"
                                            //saldoPrevisto: banco._.find($scope.bancos, function (b) { return b.addrBank == trans.senderBank }).saldoPrevisto
                                        });
                                        $scope.ultimasTransacoes = _.sortBy($scope.ultimasTransacoes, "id");
                                    }

                                }, function (err) {
                                    console.log(err);
                                });

                            });
                        //}
                    });
                });
                $q.all(buscaSaldosIniciais).then(function () {
                    $q.all(buscaUltimas).then(function () {
                        _.map($scope.ultimasTransacoes, function (trans) {
                            if (trans.permitida) {
                                var banco = _.find($scope.bancos, function (b) { return b.name == trans.nomeBancoOrigem });
                                if (!banco.debitoPrevisto) banco.debitoPrevisto = 0;
                                banco.debitoPrevisto -= trans.valorDescriptografado;
                                trans.saldoPrevisto = banco.saldoInicial + banco.saldo + banco.debitoPrevisto;
                                if (trans.saldoPrevisto < 0) trans.cor = "red";
                            }
                            return trans;
                        });
                    });
                });
            });


        });

    }

    $scope.bloquearTransacao = function (permitida, id) {
        var funcao = "blockTransaction";
        if (!permitida) funcao = "unblockTransaction";

        TransactionService.executaTransacao(nomeUsuarioBCB,
            enderecoBCB,
            $scope.senhaCarteiraBCB,
            funcao,
            JSON.stringify({
                _transaction: id
            })).then(function (data) {
                $scope.calculaSaldos();
                //$scope.valor = "";
                //$scope.senhaCarteira = "";
                //buscaUltimasTransacoes();
                //buscaSaldoAtual();
            })

    }

});