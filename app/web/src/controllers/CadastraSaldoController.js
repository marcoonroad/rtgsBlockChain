salt.controller('CadastraSaldoController', function ($scope, $state, StateService, $rootScope, TransactionService, $q) {

    var buscaIFs = function () {

        $scope.bancos = [];

        StateService.getFullState().then(function (data) {

            $scope.bancos = data.banksWeb;

        });

    }

    buscaIFs();

    $scope.cadastraSaldo = function () {
        var certificados = {};
        var saldoCripto = {};
        var getCertificates = _.map(
            [
                $scope.bancoOrigemSelecionado,
                enderecoBCB
            ], function (item) {

                return StateService.getMappingState("banks", item).then(function (data) {
                    certificados[item] = data.certificate.replace(/\u0000/g, '');

                });

            });

        $q.all(getCertificates).then(function () {

            var getState = _.map(
                [
                    $scope.bancoOrigemSelecionado,
                    enderecoBCB
                ], function (item) {
                    return $q.when(enveloped_ENCRYPT(certificados[item], "alg_CBC", "len_128",
                        $scope.saldo)).then(function (cript) {
                            saldoCripto[item] = cript;
                        });
                });

            $q.all(getState).then(function () {

                console.log(saldoCripto[$scope.bancoOrigemSelecionado]);

                TransactionService.executaTransacao(nomeUsuarioBCB, enderecoBCB, $scope.senhaCarteiraBCB, "updateBalance",
                    JSON.stringify({
                        "_bank": $scope.bancoOrigemSelecionado,
                        "_encryptedBalanceForBank": saldoCripto[$scope.bancoOrigemSelecionado],
                        "_encryptedBalanceForCentralBank": saldoCripto[enderecoBCB]
                    })
                ).then(function (ret) {
                    console.log("Cadastrado");
                }, function (err) {
                    console.log(err);
                });
            });
        });
    }

});