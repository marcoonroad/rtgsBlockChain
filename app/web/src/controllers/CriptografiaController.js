salt.controller('CriptografiaController', function ($scope, $q) {

    $scope.chaveSimetricaCriptografada = 'MIIB7AYJKoZIhvcNAQcDoIIB3TCCAdkCAQIxggFuMIIBagIBADAjMB4xHDAJBgNV' +
        'BAYTAlJVMA8GA1UEAx4IAFQAZQBzAHQCAQEwPAYJKoZIhvcNAQEHMC+gDzANBglg' +
        'hkgBZQMEAgMFAKEcMBoGCSqGSIb3DQEBCDANBglghkgBZQMEAgMFAASCAQCecLtB' +
        'bcdMbysJ47OC1qRIbYuMnSrseSdBMvwYtkR5HUPALoM8wAQkRNtMYu/zoD+tm97Q' +
        'NKUUIRnhxhK6NQHrQ7b2bfuqxbwQZBpSO2LzfyvYCnDqAyKdqkvErgyCDFv2HZTx' +
        'yqPV5jzHTD01k+LJgB40SrdW5xbJMeqDmVHMliJcTkvm1H7f9uDYXz0xtUYY6sEm' +
        's3bupH/5nt+O1CSLnFdS6ahLDeIv+sqVPQqClRsZGG98RkYR7kBx53jntxbcwwTf' +
        'lTXF7TdhYI2eKhmXpOwjCUQQ00vZOLs+i+wykJkWYADr5MtV2kSgw3y6wOTsi/pQ' +
        '1IRwMn8Ve8y37+QqMIAGCSqGSIb3DQEHATAdBglghkgBZQMEAQIEEFJovNv0wZkJ' +
        'hcEx03nxbqmggAQw8fzTecOND/Yk9gZq1buCE51FMkmegwragbAisMQmXZIdV5Ur' +
        'AARhIxyhZ6qq37ZgAAAAAA==';

    $scope.chavePublica = 'MIICxjCCAbACAQEwCwYJKoZIhvcNAQEFMBoxGDAJBgNVBAYTAlJVMAsGA1UEAxME' + 
'VGVzdDAeFw0xMzAyMDEwMDAwMDBaFw0xNjAyMDEwMDAwMDBaMBoxGDAJBgNVBAYT' + 
'AlJVMAsGA1UEAxMEVGVzdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB' + 
'AKmH8/b4p1axh5xKlGCM9ddqQs+6F1Kla3XgG0PLDUzGO1eiSNRLx+Y758RdkTwo' + 
'VcjPk90rfu4FL9TrHYdoVt8453nrCIajsKToOxOOS3e/464ngHwiuiVhtBk+q50n' + 
'HXkA1nnIjlW5a3PFf8mlZrTxr/Are+H1qWr5uRkO8PgX70P+tdjQut5u84NazMUl' + 
'gdfBcM9x0CR1xAxIGdbgu9I/N9KD0B5Q4tAm7pnl/MavwIAGQpSz8Th9ExMxpwkE' + 
'ZoTHkaUGaO1xWYBEhnFVwficsvFUCO3XQ5NnU5lfnf4qTyr0mHsonypnk8e1fQ4d' + 
'l95HovZg46wIUp+Mk2/KglECAwEAAaMgMB4wDwYDVR0TBAgwBgEB/wIBAzALBgNV' + 
'HQ8EBAMCAAYwCwYJKoZIhvcNAQEFA4IBAQB5IX9QSJ+gvdd8P6mJBONRUp4q0qex' + 
'gkMSa9X0meb8vUfUyrfVjP7t2rldf7Sw/9aHDY0i7/VnNgt4sFSyqURrvCw82mQQ' + 
'9QaOvE7pqhUDMyE3thDmbvnlJK6AzhV/eeBz/wYw2pOA5X+XXQeQ/7NEjF0tOhMM' + 
'KhD+FndAkF/dC0CbtR631qI4oRWwjYc7NxFLt28dBRgluz7LTOwz55TkZ8aZuPEz' + 
'f/9RSWGbcl93Oy5IgD5f+TJ8n+5Ef3fUQK1Au3XAxRbRhcqGlKiLWVp0KMBsBzbL' + 
'24pzqdf+J9Zu8QuGvyeyRjb/RvO9iqttkydxwE1CvZquM2yGg+6X4/VS';

    $scope.chavePrivada = 'MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDQpvf7J2Rm/fFZ' +
        '4feRyHY2M2h2KLknPODSqbwRjwAoEwXof1iwR+Wh3e8Y+ELk/xXslTj/Xaea0MH/' +
        'Jdj+wnHaSu2VeYW0GK8I8HmLjEkf1G2h6Q9VCXJpHXVzkffz5fi0+9HHbaCK6NOQ' +
        'fi1MJfnov8rdhEH+VKdMDw5zaU7bwsLPo2rOp9TIo5hPX0eRg0PHX8+BJP2DLulY' +
        'sW67bam0CUxFRPTeOrkwLkrxHMTO7J4xKS3AjtJBmi9H4xFCcxGvZZq/0YuzwjQo' +
        '7/nPs1st+60JBRSUMxPMwwSO89JyF3qkad++FEocH0+xHASIFEpOXRSs0Th7AEjz' +
        'twU/EfFTAgMBAAECggEBAK6J3YrkquJUrx5HHjd+/xPb8RhWfHvMOy09+yqc+r6L' +
        'GzgfvurHWdoLssSqeBrjH03mjeVgTKH+2yk6WcjSkzbN0uxk2WmL9G9/6czILytH' +
        'IeZ89OxnmKNBl0xz4HzP9TpAJH20wMUyeje2tOGsKt9+AI9I4YOFAOVo+uekQekm' +
        'AZmMQETm64KBHFCeKVNy3h5O2swCh+zOi9/u2wXZqwhuPeos4RJW+ujxxbiDJV4u' +
        'p8DHaHFt+9YLwcr31RB5q12669NjiXlk9ae0fpk8CMUXgYkmwUCvwxKoVZX6ue60' +
        '18qFbPsrsviXDjIBUIYQciXJOy0xVdJ/N5R1H6wfb1ECgYEA8RbgGUHPBaXsCSNt' +
        'Ctx73p2P6odnFedYBYXaj6MR2/czDL40VTZN2lQjC+sJYrPPI50LxsbTkwtECbv2' +
        'TqMhn+yy9l0jcCgQWBZo2sCc0T6N4dp3aHuyxBaJadc/iDoE9fkPhmvIdx81klR9' +
        'nFFw6Gu0z+2+RskCGfMy2TvZFTUCgYEA3Y6FnVLyamUlCk1vTzz4mCjrmVnf4adC' +
        'WrEyGk/9XEFRiOxfxyMLlOun9PFk+Nh6oSSsNp843TlYfdivukfodjyj0p0XVsfW' +
        'gvZ9kNdR7XC/xXHGxVx1bEA088aeIM1DqM5eo1LMHiF3jbUoZJyBx0zIaPPzaOv5' +
        '0mBObtH65WcCgYEA09j6CredJqVqB42tn8GmxzNE5x0cJE8pNZEtQNJWOye5IBVy' +
        '/v2844DTWwGpOaYu+mhhlMXZ3HglzTfxc6A9R75Cc+Hb5g4ObpkigjaGlgeVvUSv' +
        'cIxhxZTYIr0WAqqY+VtlnQTRHnu6I19DET0vLcyXEa8jaCfxRMRDhkfktZECgYEA' +
        'yBAJcV3UTuTUhLAVwhyRcn0I+enhM1egoFp/nqj6ZNMKuDSR7DUmISyG9rT/84Ev' +
        'gtGlvJEcfQ9qbAE99V03eYUafXWi/Jbpikrgaek1Ls5wLO+niNILqlt0NWj1ozMX' +
        'VJg9dweuD8sytMCQIAH/YqNgGP2on+buKN0UFhgAkiMCgYEAxOO3lSf0DTjqzA5e' +
        'wO4DHa7If35rIPEkoSVGSvtN2nLo4T3c5p+X5UR7F6SrWrqfizk0bWtKBKsmapmp' +
        'LmBy+T326dHCcwMRplgntc1K9dDJi969duGcpur5NZaprS31/HwL3O/0QnB8hMU0' +
        'vuFw0+VwWOVAvOmMsQAsqa4aeAQ=';

    $scope.criptografarAss = function () {

        $q.when(enveloped_ENCRYPT($scope.chavePublica, "alg_CBC", "len_128", $scope.texto)).then(function (data) {
            $scope.mensagemCripografada = data;
        }, function (err) {
            console.log(err);
        })
    }

    $scope.descriptografarAss = function () {
        $q.when(enveloped_DECRYPT($scope.chavePublica, $scope.chavePrivada, $scope.mensagemCripografada)).then(function (data) {
            $scope.mensagemDescriptografada = data;
        }, function (err) {
            console.log(err);
        });
    }

    $scope.criptografarSim = function () {
        $scope.mensagemCripografada = CryptoJS.AES.encrypt($scope.texto, "12345").toString();
    }
    $scope.descriptografarSim = function () {
        $scope.mensagemDescriptografada = CryptoJS.AES.decrypt($scope.mensagemCripografada, "12345").toString(CryptoJS.enc.Utf8);
    }


});