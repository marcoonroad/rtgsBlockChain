salt.service("StateService", function ($http) {

    this.getFullState = function () {

        var url = "/contracts/" + nomeContrato + "/" + enderecoContrato + "/state";
        return $http({
            method: 'GET',
            url: url,
        }).then(function (ret) {
            //console.log(data);
            //var state = JSON.parse(ret);
            return ret.data;

        });
    }

    this.getMappingState = function (nome, chave) {

        var url = "/contracts/" + nomeContrato + "/" + enderecoContrato + "/state/mapping/" + nome + "/" + chave;
        return $http({
            method: 'GET',
            url: url,
        }).then(function (ret) {
            //console.log(data);
            //var state = JSON.parse(ret);
            return ret.data;

        });

    };

    this.getMappingMappingState = function (nome, chave, outroNome, outraChave) {

        var url = "/contracts/" + nomeContrato + "/" + enderecoContrato + "/state/mapping/" + nome + "/" + chave + "/mapping/" + outroNome + "/" + outraChave;
        return $http({
            method: 'GET',
            url: url,
        }).then(function (ret) {
            //console.log(data);
            //var state = JSON.parse(ret);
            return ret.data;

        });

    };
})