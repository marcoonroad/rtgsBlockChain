salt.service("TransactionService", function ($http) {

    this.executaTransacao = function (nomeIF, enderecoIF, password, nomeFuncao, parametros) {

        var url = urlBase + nomeIF + "/" + enderecoIF + "/contract/" +
            nomeContrato + "/" + enderecoContrato + "/call";

        return $http({
            method: 'POST',
            url: url,
            data: {
                password: password,
                method: nomeFuncao,
                args: parametros
            }
        }).then(function (ret) {
            return ret;
        });
    }

});