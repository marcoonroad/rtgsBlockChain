//*********************************************************************************
var certificateBuffer = new ArrayBuffer(0); // ArrayBuffer with loaded or created CERT 
var trustedCertificates = new Array(); // Array of root certificates from "CA Bundle"
var intermadiateCertificates = new Array(); // Array of intermediate certificates
var crls = new Array(); // Array of CRLs for all certificates (trusted + intermediate)
//*********************************************************************************
// #region Auxiliary functions 
//*********************************************************************************
function formatPEM(pem_string) {
    /// <summary>Format string in order to have each line with length equal to 63</summary>
    /// <param name="pem_string" type="String">String to format</param>

    var string_length = pem_string.length;
    var result_string = "";

    for (var i = 0, count = 0; i < string_length; i++ , count++) {
        if (count > 63) {
            result_string = result_string + "\r\n";
            count = 0;
        }

        result_string = result_string + pem_string[i];
    }

    return result_string;
}
//*********************************************************************************
function arrayBufferToString(buffer) {
    /// <summary>Create a string from ArrayBuffer</summary>
    /// <param name="buffer" type="ArrayBuffer">ArrayBuffer to create a string from</param>

    var result_string = "";
    var view = new Uint8Array(buffer);

    for (var i = 0; i < view.length; i++)
        result_string = result_string + String.fromCharCode(view[i]);

    return result_string;
}
//*********************************************************************************
function stringToArrayBuffer(str) {
    /// <summary>Create an ArrayBuffer from string</summary>
    /// <param name="str" type="String">String to create ArrayBuffer from</param>

    var stringLength = str.length;

    var resultBuffer = new ArrayBuffer(stringLength);
    var resultView = new Uint8Array(resultBuffer);

    for (var i = 0; i < stringLength; i++)
        resultView[i] = str.charCodeAt(i);

    return resultBuffer;
}
//*********************************************************************************
// #endregion 
//*********************************************************************************
// #region Create CERT  
//*********************************************************************************
function create_CERT(buffer) {
    // #region Initial variables 
    var sequence = Promise.resolve();

    var cert_simpl = new org.pkijs.simpl.CERT();
    var cms_signed_simpl;

    var publicKey;
    var privateKey;

    var hash_algorithm;
    var hash_option = document.getElementById("hash_alg").value;
    switch (hash_option) {
        case "alg_SHA1":
            hash_algorithm = "sha-1";
            break;
        case "alg_SHA256":
            hash_algorithm = "sha-256";
            break;
        case "alg_SHA384":
            hash_algorithm = "sha-384";
            break;
        case "alg_SHA512":
            hash_algorithm = "sha-512";
            break;
        default: ;
    }

    var signature_algorithm_name;
    var sign_option = document.getElementById("sign_alg").value;
    switch (sign_option) {
        case "alg_RSA15":
            signature_algorithm_name = "RSASSA-PKCS1-V1_5";
            break;
        case "alg_RSA2":
            signature_algorithm_name = "RSA-PSS";
            break;
        case "alg_ECDSA":
            signature_algorithm_name = "ECDSA";
            break;
        default: ;
    }
    // #endregion 

    // #region Get a "crypto" extension 
    var crypto = org.pkijs.getCrypto();
    if (typeof crypto == "undefined") {
        alert("No WebCrypto extension found");
        return;
    }
    // #endregion 

    // #region Put a static values 
    //TODO: Isso pode ser parametro
    cert_simpl.version = 2;
    cert_simpl.serialNumber = new org.pkijs.asn1.INTEGER({ value: 1 });
    cert_simpl.issuer.types_and_values.push(new org.pkijs.simpl.ATTR_TYPE_AND_VALUE({
        type: "2.5.4.6", // Country name
        value: new org.pkijs.asn1.PRINTABLESTRING({ value: "RU" })
    }));
    cert_simpl.issuer.types_and_values.push(new org.pkijs.simpl.ATTR_TYPE_AND_VALUE({
        type: "2.5.4.3", // Common name
        value: new org.pkijs.asn1.BMPSTRING({ value: "Test" })
    }));
    cert_simpl.subject.types_and_values.push(new org.pkijs.simpl.ATTR_TYPE_AND_VALUE({
        type: "2.5.4.6", // Country name
        value: new org.pkijs.asn1.PRINTABLESTRING({ value: "RU" })
    }));
    cert_simpl.subject.types_and_values.push(new org.pkijs.simpl.ATTR_TYPE_AND_VALUE({
        type: "2.5.4.3", // Common name
        value: new org.pkijs.asn1.BMPSTRING({ value: "Test" })
    }));

    cert_simpl.notBefore.value = new Date(2013, 01, 01);
    cert_simpl.notAfter.value = new Date(2016, 01, 01);

    cert_simpl.extensions = new Array(); // Extensions are not a part of certificate by default, it's an optional array

    // #region "BasicConstraints" extension
    //var basic_constr = new org.pkijs.simpl.x509.BasicConstraints({
    //    cA: true,
    //    pathLenConstraint: 3
    //});

    //cert_simpl.extensions.push(new org.pkijs.simpl.EXTENSION({
    //    extnID: "2.5.29.19",
    //    critical: false,
    //    extnValue: basic_constr.toSchema().toBER(false),
    //    parsedValue: basic_constr // Parsed value for well-known extensions
    //}));
    // #endregion 

    // #region "KeyUsage" extension 
    var bit_array = new ArrayBuffer(1);
    var bit_view = new Uint8Array(bit_array);

    bit_view[0] = bit_view[0] | 0x02; // Key usage "cRLSign" flag
    //bit_view[0] = bit_view[0] | 0x04; // Key usage "keyCertSign" flag

    var key_usage = new org.pkijs.asn1.BITSTRING({ value_hex: bit_array });

    cert_simpl.extensions.push(new org.pkijs.simpl.EXTENSION({
        extnID: "2.5.29.15",
        critical: false,
        extnValue: key_usage.toBER(false),
        parsedValue: key_usage // Parsed value for well-known extensions
    }));
    // #endregion 
    // #endregion 

    // #region Create a new key pair 
    sequence = sequence.then(
        function () {
            // #region Get default algorithm parameters for key generation 
            var algorithm = org.pkijs.getAlgorithmParameters(signature_algorithm_name, "generatekey");
            if ("hash" in algorithm.algorithm)
                algorithm.algorithm.hash.name = hash_algorithm;
            // #endregion 

            return crypto.generateKey(algorithm.algorithm, true, algorithm.usages);
        }
    );
    // #endregion 

    // #region Store new key in an interim variables
    sequence = sequence.then(
        function (keyPair) {
            publicKey = keyPair.publicKey;
            privateKey = keyPair.privateKey;
        },
        function (error) {
            alert("Error during key generation: " + error);
        }
    );
    // #endregion 

    // #region Exporting public key into "subjectPublicKeyInfo" value of certificate 
    sequence = sequence.then(
        function () {
            return cert_simpl.subjectPublicKeyInfo.importKey(publicKey);
        }
    );
    // #endregion 

    // #region Signing final certificate 
    sequence = sequence.then(
        function () {
            return cert_simpl.sign(privateKey, hash_algorithm);
        },
        function (error) {
            alert("Error during exporting public key: " + error);
        }
    );
    // #endregion 

    // #region Encode and store certificate 
    sequence = sequence.then(
        function () {
            certificateBuffer = cert_simpl.toSchema(true).toBER(false);

            var cert_simpl_string = String.fromCharCode.apply(null, new Uint8Array(certificateBuffer));

            var result_string = "-----BEGIN CERTIFICATE-----\r\n";
            result_string = result_string + formatPEM(window.btoa(cert_simpl_string));
            result_string = result_string + "\r\n-----END CERTIFICATE-----\r\n";

            document.getElementById("new_signed_data").innerHTML = result_string;

            alert("Certificate created successfully!");
        },
        function (error) {
            alert("Error during signing: " + error);
        }
    );
    // #endregion 

    // #region Exporting private key 
    sequence = sequence.then(
        function () {
            return crypto.exportKey("pkcs8", privateKey);
        }
    );
    // #endregion 

    // #region Store exported key on Web page 
    sequence = sequence.then(
        function (result) {
            var private_key_string = String.fromCharCode.apply(null, new Uint8Array(result));

            var result_string = "";

            result_string = result_string + "-----BEGIN PRIVATE KEY-----\r\n";
            result_string = result_string + formatPEM(window.btoa(private_key_string));
            result_string = result_string + "\r\n-----END PRIVATE KEY-----\r\n";

            document.getElementById("pkcs8_key").innerHTML = result_string;

            alert("Private key exported successfully!");
        },
        function (error) {
            alert("Error during exporting of private key: " + error);
        }
    );
    // #endregion 
}
//*********************************************************************************
// #endregion 
//*********************************************************************************
// #region Encrypt input data 
//*********************************************************************************
function enveloped_ENCRYPT(certificate, encryptionAlgorithmSelect, encryptionAlgorithmLengthSelect, content) {
    var deferred = Q.defer();
    // #region Decode input certificate 
    var encodedCertificate = certificate;
    var clearEncodedCertificate = encodedCertificate.replace(/(-----(BEGIN|END)( NEW)? CERTIFICATE-----|\n)/g, '');
    var certificateBuffer = stringToArrayBuffer(window.atob(clearEncodedCertificate));

    var asn1 = org.pkijs.fromBER(certificateBuffer);
    var cert_simpl = new org.pkijs.simpl.CERT({ schema: asn1.result });
    // #endregion 

    // #region Create WebCrypto form of content encryption algorithm 
    var encryptionAlgorithm = {};

    //var encryptionAlgorithmSelect = document.getElementById("content_enc_alg").value;
    switch (encryptionAlgorithmSelect) {
        case "alg_CBC":
            encryptionAlgorithm.name = "AES-CBC";
            break;
        case "alg_GCM":
            encryptionAlgorithm.name = "AES-GCM";
            break;
        default: ;
    }

    //var encryptionAlgorithmLengthSelect = document.getElementById("content_enc_alg_len").value;
    switch (encryptionAlgorithmLengthSelect) {
        case "len_128":
            encryptionAlgorithm.length = 128;
            break;
        case "len_192":
            encryptionAlgorithm.length = 192;
            break;
        case "len_256":
            encryptionAlgorithm.length = 256;
            break;
        default: ;
    }
    // #endregion 

    var cmsEnveloped = new org.pkijs.simpl.CMS_ENVELOPED_DATA();

    cmsEnveloped.addRecipientByCertificate(cert_simpl);

    cmsEnveloped.encrypt(encryptionAlgorithm, stringToArrayBuffer(content)).
        then(
        function (result) {
            var cms_content_simpl = new org.pkijs.simpl.CMS_CONTENT_INFO();
            cms_content_simpl.contentType = "1.2.840.113549.1.7.3";
            cms_content_simpl.content = cmsEnveloped.toSchema();

            var schema = cms_content_simpl.toSchema();
            var ber = schema.toBER(false);

            var ber_string = String.fromCharCode.apply(null, new Uint8Array(ber));

            //var result_string = ""; //g = "-----BEGIN CMS-----\r\n";
            var result_string = formatPEM(window.btoa(ber_string));
            //result_string = result_string + "\r\n-----END CMS-----\r\n";

            deferred.resolve(result_string);

            //document.getElementById("encrypted_content").innerHTML = result_string;

            //alert("Encryption process finished successfully");
        },
        function (error) {
            deferred.reject(error);
            //alert("ERROR DURING ENCRYPTION PROCESS: " + error);
        }
        );

    return deferred.promise;
}
//*********************************************************************************
// #endregion 
//*********************************************************************************
// #region Decrypt input data 
//*********************************************************************************
function enveloped_DECRYPT(encodedCertificate, encodedPrivateKey, encodedCMSEnveloped) {

    var deferred = Q.defer();
    // #region Decode input certificate 
    //var encodedCertificate = document.getElementById("new_signed_data").innerHTML;
    var clearEncodedCertificate = encodedCertificate.replace(/(-----(BEGIN|END)( NEW)? CERTIFICATE-----|\n)/g, '');
    var certificateBuffer = stringToArrayBuffer(window.atob(clearEncodedCertificate));

    var asn1 = org.pkijs.fromBER(certificateBuffer);
    var cert_simpl = new org.pkijs.simpl.CERT({ schema: asn1.result });
    // #endregion 

    // #region Decode input private key 
    //var encodedPrivateKey = document.getElementById("pkcs8_key").innerHTML;
    var clearPrivateKey = encodedPrivateKey.replace(/(-----(BEGIN|END)( NEW)? PRIVATE KEY-----|\n)/g, '');
    var privateKeyBuffer = stringToArrayBuffer(window.atob(clearPrivateKey));
    // #endregion 

    // #region Decode CMS Enveloped content 
    //var encodedCMSEnveloped = document.getElementById("encrypted_content").innerHTML;
    var clearEncodedCMSEnveloped = encodedCMSEnveloped.replace(/(-----(BEGIN|END)( NEW)? CMS-----|\n)/g, '');
    var cmsEnvelopedBuffer = stringToArrayBuffer(window.atob(clearEncodedCMSEnveloped));

    var asn1 = org.pkijs.fromBER(cmsEnvelopedBuffer);
    var cms_content_simpl = new org.pkijs.simpl.CMS_CONTENT_INFO({ schema: asn1.result });
    var cms_enveloped_simp = new org.pkijs.simpl.CMS_ENVELOPED_DATA({ schema: cms_content_simpl.content });
    // #endregion 

    cms_enveloped_simp.decrypt(0,
        {
            recipientCertificate: cert_simpl,
            recipientPrivateKey: privateKeyBuffer
        }).then(
        function (result) {
            //document.getElementById("decrypted_content").innerHTML = arrayBufferToString(result);
            deferred.resolve(arrayBufferToString(result));
        },
        function (error) {
            //alert("ERROR DURING DECRYPTION PROCESS: " + error);
            deferred.reject(error);
        }
        );
    return deferred.promise;
}
        //*********************************************************************************
        // #endregion 
        //*********************************************************************************