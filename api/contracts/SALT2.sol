
contract BlockchainRTGSv2 {
 
 
address public centralBank;
bool public operational; 
uint public confirmationBlocks;
uint public totalTransactions;
 
struct Balance {
    bool allowed;
    bool exists;
    int relativeBalance;
    string name;
    string encryptedBalanceForBank;
    string encryptedBalanceForCentralBank;
}

struct Bank{
    address addrBank;
    string name;
}

struct Transaction {
    uint blockNumber;
    address senderBank;
    address rcptBank;
    int value;
    bool confirmed;
    bool allowed;
}
 
 
mapping (address => Balance) public balances;
mapping (uint => Transaction) public transactionLog;
mapping(address => uint[]) public bankTransactions;


Bank[] public banks;
 
 
function BlockchainRTGSv2(){
 
    centralBank=msg.sender;
    operational=false;
    totalTransactions = 0;
    confirmationBlocks = 10; 
    Balance memory balance = Balance(true, true, 0, "BCB", "", "");
    balances[centralBank] = balance;  
}
 
modifier onlyOwner() { if (msg.sender != centralBank) throw;_
}
 
modifier onlyIfAllowed() { if (!balances[msg.sender].allowed) throw;_
}

function makeOperational() onlyOwner {
    operational=true;
}

function changeConfirmationBlocks(uint _confirmationBlocks) onlyOwner {
    confirmationBlocks=_confirmationBlocks;
}

function addBank (address _bank, string _name) onlyOwner returns (uint) {

    Balance memory balance = Balance(true, true, 0, _name, "", "");
    balances[_bank] = balance;
    banks.push(Bank(_bank,_name));
    return banks.length;
    
}

function getMyInformation() onlyIfAllowed returns (string, int){
    return (balances[msg.sender].name, balances[msg.sender].relativeBalance);
}

function updateBalance(address _bank, string _encryptedBalanceForCentralBank, 
string _encryptedBalanceForBank) onlyOwner {
    balances[_bank].encryptedBalanceForCentralBank = _encryptedBalanceForCentralBank;
    balances[_bank].encryptedBalanceForBank = _encryptedBalanceForBank;
}
 
function createTransaction(address _rcptBank, int _value) onlyIfAllowed returns (int){
 
    if (!balances[_rcptBank].exists) return -1;
    if (!balances[msg.sender].exists) return -2;
    if (!operational) return -3;

    totalTransactions++;
    bankTransactions[msg.sender].push(totalTransactions);

    transactionLog[totalTransactions] = Transaction(block.number,msg.sender,_rcptBank,_value,false,true);
    return 0;
}

function getTransaction(uint _transaction) returns (int,uint,bool,bool){
    Transaction trans = transactionLog[_transaction];
    return (trans.value,trans.blockNumber,trans.allowed,trans.confirmed);

}

function confirmTransaction(uint _transaction) onlyIfAllowed returns (int){
    if ((msg.sender != transactionLog[_transaction].senderBank) &&
        (msg.sender != centralBank)) return -1;
    if (!transactionLog[_transaction].allowed) return -2;

    if (block.number-transactionLog[_transaction].blockNumber < confirmationBlocks) return -3;
    
    balances[transactionLog[_transaction].senderBank].relativeBalance -= transactionLog[_transaction].value;
    balances[transactionLog[_transaction].rcptBank].relativeBalance += transactionLog[_transaction].value;
    transactionLog[_transaction].confirmed=true;
    return 0;
}

function blockBank(address _bankToBlk) onlyOwner {
    balances[_bankToBlk].allowed = false;
}

function unblockBank(address _bankToBlk) onlyOwner {
    balances[_bankToBlk].allowed = true;
}

function blockTransaction(uint _transaction) onlyOwner {
    transactionLog[_transaction].allowed = false;
}

function unblockTransaction(uint _transaction) onlyOwner {
    transactionLog[_transaction].allowed = true;
}



function kill() onlyOwner {
    suicide(centralBank);
}
}
